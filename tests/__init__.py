import unittest

from orderedpy import Ordered


class TestOrdered(unittest.TestCase):
    def setUp(self):
        self.o = Ordered([2, 1, 3, 4, 2, 3])
        self.sorted = [1, 2, 2, 3, 3, 4]

    def test_is_sorted(self):
        self.assertTrue(self.o._container == self.sorted)

    def test_insert(self):
        self.assertRaises(IndexError, self.o.insert, 0, 4)

    def test_append(self):
        self.o.append(3)
        self.o.append(5)
        self.assertTrue(self.o == [1, 2, 2, 3, 3, 3, 4, 5])

    def test_compare(self):
        other = [1, 2, 3]
        self.assertFalse(self.o == other)

    def test_compare_ordered(self):
        other = Ordered([1, 2, 3])
        self.assertFalse(self.o == other)

    def test_iter(self):
        copy = []
        for i in self.o:
            copy.append(i)
        self.assertTrue(self.o == copy)

    def test_all_indecies(self):
        b, e = self.o.all_indecies(3)
        self.assertTrue(b == 3 and e == 4)

    def test_setitem(self):
        self.o[1] = 1
        self.assertTrue(self.o == [1, 1, 2, 3, 3, 4])

    def test_with_tuples(self):
        t = Ordered([(1, 2), (3, 1), (2, 4)], key=lambda a: a[1])
        self.assertTrue(t == [(3, 1), (1, 2), (2, 4)])

    def test_with_reverse(self):
        result = []
        o = Ordered([1, 2, 1, 2, 3], reverse=True)
        result.append(o == [3, 2, 2, 1, 1])
        o.append(5)
        result.append(o == [5, 3, 2, 2, 1, 1])
        o[0] = 10
        result.append(o == [10, 3, 2, 2, 1, 1])
        self.assertTrue(len(list(filter(lambda r: r == True, result))) == len(result))
